.. _partner:

Partner verwalten
==================
Um Partner zu verwalten, navigieren Sie im :ref:`Backend <login>` zu *Partner*.

Partner werden direkt auf der Webseite dargestellt und können bei der :ref:`Projektverwaltung <project>` referenziert werden.

Partnerkategorie
----------------
Um Partnerkategorien zu verwalten,  navigieren Sie im :ref:`Backend <login>` zu *Projectpartners/Categories*

.. _new_partnercategory:

Hinzufügen
~~~~~~~~~~
Füllen Sie hier das Formular auf der linken Seite aus und klicken anschließend auf "Add New Category"

.. image:: _static/images/partner/new_category.png

Die neue Partnerkategorie erscheint nun auf der rechten Seite.

Löschen/bearbeiten
~~~~~~~~~~~~~~~~~~

Fahren Sie mit der Maus auf einen Bestehenden Eintrag und wählen sie *Bearbeiten* oder *Löschen* aus.

.. image:: _static/images/partner/edit_delete.png

Partner
-------

Hinzufügen
~~~~~~~~~~

Um Partner hinzuzufügen, navigieren Sie im :ref:`Backend <login>` zu *Partner/Add New*.

Füllen Sie zunächst die gewohnten Felder *Titel* und *Inhalt* aus. Achten Sie hierbei darauf, den Text nicht zu lange zu gestalten, da sonst die Lesbarkeit auf der Webseite eingeschränkt wird. Wenige Absätze sind zu empfehlen.

.. image:: _static/images/partner/new_main.png

Füllen Sie anschließend die Box *Contact* aus. Diese Informationen werden auch bei den :ref:`Projekten <project>` dargestellt.

.. image:: _static/images/partner/contact.png

Weisen Sie dem Partner außerdem (falls vorhanden) ein :ref:`Logo <media_choose_image>` und eine :ref:`Kategorie <new_partnercategory>` zu.

.. image:: _static/images/partner/logo_category.png

Löschen
~~~~~~~

ToDo
