.. _newsletter:

Newsletter verwalten
====================
Newsletter bestehen und Newsletter Teilen (hier: *NL Section*) und kompletten Ausgaben (hier: *Newsletter*)
Um diese zu verwalten, navigieren Sie im :ref:`Backend <login>` zu *NL Sections*.


Newsletter
----------
Um Newsletter Ausgaben zu verwalten,  navigieren Sie im :ref:`Backend <login>` zu *NL Sections/Newsletter*

Hinzufügen
~~~~~~~~~~
Füllen Sie hier das Formular auf der linken Seite aus und klicken anschließend auf "Add New Newsletter"

Wählen Sie dabei einen prägnanten Wert für *Permalink*. Dies ist der Teil in der URL, die auf den Newsletter verweisen wird. Lautet der Permalink etwa **4** so entsteht dafür die URL: http://en.danube-networkers.eu/newsletters/**4**/

.. image:: _static/images/newsletter/new_newsletter.png

Der neue Newsletter erscheint nun auf der rechten Seite.

Löschen/bearbeiten
~~~~~~~~~~~~~~~~~~
Fahren Sie mit der Maus auf einen Bestehenden Eintrag und wählen sie *Bearbeiten* oder *Löschen* aus.

.. image:: _static/images/newsletter/edit_delete.png

Newsletter Teil (NL Section) 
----------------------------

Hinzufügen
~~~~~~~~~~
Um Newsletter Teile hinzuzufügen, navigieren Sie im :ref:`Backend <login>` zu *NL Sections/Add New*.

Füllen Sie hier die Felder wir gewohnt aus.

Wählen Sie im rechten Bereich unter *Settings* den zuvor angelegten Newsletter aus. Setzen Sie dabei auch die Reihenfolge des Newsletter teils auf den Gewünschten Wert (z.B. 2.3)

.. image:: _static/images/newsletter/section.png

Wählen Sie anschließend *Veröffentlichen*

Löschen
~~~~~~~

ToDo
