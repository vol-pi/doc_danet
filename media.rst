.. _media:

Media
=====

Um eine Übersicht der auf der Webseite vorhandenen Bilder zu erhalten,  navigieren Sie im :ref:`Backend <login>` zu *Medien*.

Dateien hinzufügen
------------------

Um Dateien hinzuzufügen, navigieren Sie im :ref:`Backend <login>` zu *Medien/Datei hinzufügen*.

Ziehen Sie anschließend die hochzuladende Datei aus Ihrem Windows Ordner in Ihr Browserfenster.

.. image:: _static/images/media/upload.png

.. _media_choose_image:

Datei auswählen
---------------

Klicken Sie in der Bildauswahl einmal auf das gewünschte Bild.

.. image:: _static/images/media/choose.png
